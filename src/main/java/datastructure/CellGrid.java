package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    private int rows;
    private int cols;
    private CellState initialState;
    private cellular.CellState[][] state;
    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows=rows;
        this.cols=columns;
        state=new CellState[rows][columns];
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return this.rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if((row<0||row>this.rows)||(column<0||column>this.cols)){
            throw new IndexOutOfBoundsException("Not in range");
        }
        else{
            this.state[row][column]=element;
        }
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if((row<0||row>this.rows)||(column<0||column>this.cols)){
            throw new IndexOutOfBoundsException("Not in range");
        }
        else{
            return this.state[row][column];
        }
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        IGrid copy=new CellGrid(numRows(), numColumns(), initialState);
        for(int r=0;r<numRows();r++){
            for(int c=0;c<numColumns();c++){
                copy.set(r, c,get(r,c));
            }
        }
        return copy;
    }
    
}
