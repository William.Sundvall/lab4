package cellular;

import java.util.Random;
import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {
    IGrid currentGeneration;

    public BriansBrain(int rows, int columns){
        currentGeneration=new CellGrid(rows,columns,CellState.ALIVE);
        initializeCells();
    }
    @Override
    public CellState getCellState(int row, int column){
        return currentGeneration.get(row,column);
    }
    private int countNeighbors(int row, int col, CellState state) {
        int count=0;
        for (int r=row-1;r<=row+1;r++){
            for(int c=col-1;c<=col+1;c++){
                if(c<0||r<0||c>=numberOfColumns()||r>=numberOfRows()||(c==row&&r==col)){
                    continue;
                }
                else if(getCellState(r, c)==state){
                    count++;
                }
            }
        }
        return count;
    }
    @Override
    public void initializeCells(){
        Random random= new Random();
        for(int row=0;row<numberOfRows();row++){
            for(int col=0; col<numberOfColumns();col++){
                if(random.nextBoolean()){
                    currentGeneration.set(row, col, CellState.ALIVE);
                }
                else{
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
        }   
    }
    @Override
        public int numberOfColumns(){
            return currentGeneration.numColumns();
        }
            
    @Override
    public int numberOfRows(){
        return currentGeneration.numRows();
    }
    
    @Override
    public void step(){
        IGrid nextGeneration=currentGeneration.copy();
        for(int r=0;r<numberOfRows();r++){
            for(int c=0;c<numberOfColumns();c++){
                currentGeneration.set(r,c,getNextCell(r,c));
            }
        }
        currentGeneration=nextGeneration;
    }

    @Override
    public CellState getNextCell(int row, int col) {
		CellState state= getCellState(row, col);
		if(state==CellState.ALIVE){
			return CellState.DYING;
		}
        else if(state==CellState.DYING){
            return CellState.DEAD;
        }
        else{
            if(countNeighbors(row, col, CellState.ALIVE)==2){
                return CellState.ALIVE;
            }
            else{
                return state;
            }
        }
    }

    @Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}



